import View from './../view'

class MessageBox extends View {
  constructor (message) {
    super()
    this.initialMessage = message
    this.createMessageBox(message)
  }

  createMessageBox (message) {
    this.element = this.createElement({ tagName: 'div', className: 'text-message' })
    this.set(message)
  }

  set (message) {
    this.element.innerHTML = message
  }

  reset () {
    this.set(this.initialMessage)
  }
}

export default MessageBox