import View from '../view'

class Button extends View {
  constructor (value, handler, className = '') {
    super()
    this.createButtonView(value, className)
    this.element.addEventListener('click', handler, false)
  }

  createButtonView (value, className) {
    const classList = 'button ' + className
    const attributes = { type: 'button', value: value }

    this.element = this.createElement({
      tagName: 'input',
      className: classList,
      attributes
    })
  }

  hide () {
    this.element.style.display = 'none'
  }

  show () {
    this.element.style.display = ''
  }
}

export default Button
