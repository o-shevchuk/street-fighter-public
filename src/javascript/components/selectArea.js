import View from '../view'
import FighterView from './../fighterView'
import HealthBar from './healthBar'
import App from './../app'

class SelectArea extends View {
  constructor (areaId) {
    super()
    this.areaId = areaId;
    this.handleDrop = this.handleFighterDrop.bind(this)
    this.createSelectArea(areaId)
  }

  createSelectArea (areaId) {
    this.element = this.createElement({ tagName: 'div', className: 'selected-fighter' })
    this.element.addEventListener('dragover', this.handleDragOver, false)
    this.element.addEventListener('drop', event => this.handleDrop(event, areaId))

    this.healthBar = new HealthBar()

    this.fighterImage = this.createElement({
      tagName: 'img',
      className: 'fighter-placeholder',
      attributes: { draggable: false }
    })

    this.element.appendChild(this.healthBar.element)
    this.element.appendChild(this.fighterImage)
  }

  handleDragOver (event) {
    event.preventDefault()
  }

  handleFighterDrop (event, areaId) {
    event.preventDefault()
    try{
    const data = event.dataTransfer.getData('text')
    const fighter = JSON.parse(data)
    this.fighterImage.setAttribute('src', fighter.source)
    this.currentFighterId = fighter._id

    const eventOptions = { bubbles: true, detail: { areaId, fighter } }
    const selectFighterEvent = new CustomEvent('select-fighter', eventOptions)
    this.element.dispatchEvent(selectFighterEvent)
  } catch(err){
    console.warn('Only fighters allowed to fight')
  }
  }
}

export default SelectArea
