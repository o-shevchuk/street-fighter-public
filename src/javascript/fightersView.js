import View from './view'
import FighterView from './fighterView'
import FighterDetailsView from './fighterDetailsView'
import { fighterService } from './services/fightersService'
import App from './app'

class FightersView extends View {
  constructor (fighters) {
    super()

    this.handleClick = this.handleFighterClick.bind(this)
    this.handleUpdate = this.updateFighterDetails.bind(this)
    this.handleDrag = this.handleFighterDrag.bind(this)
    this.handleSelectFighter = this.loadSelectedFighter.bind(this)

    this.createFighters(fighters)
    App.rootElement.addEventListener('update-details', this.handleUpdate, false)
    App.rootElement.addEventListener('select-fighter', this.handleSelectFighter)
  }

  fightersDetailsMap = new Map()

  createFighters (fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, { click: this.handleClick, dragstart: this.handleDrag })
      return fighterView.element
    })

    this.element = this.createElement({ tagName: 'div', className: 'fighters' })
    this.element.append(...fighterElements)
  }

  async handleFighterClick (event, fighter) {
    let fighterDetails = this.fightersDetailsMap.get(fighter._id)

    if (typeof fighterDetails === 'undefined') {
      fighterDetails = await fighterService.getFighterDetails(fighter._id)
      this.fightersDetailsMap.set(fighter._id, fighterDetails)
    }

    const detailsView = new FighterDetailsView(fighterDetails, event)
    App.rootElement.append(detailsView.element)
  }

  updateFighterDetails (event) {
    const fighter = event.detail
    this.fightersDetailsMap.set(fighter._id, fighter)
  }

  handleFighterDrag (event, fighter) {
    const fighterData = JSON.stringify(fighter)
    event.dataTransfer.setData('text/plain', fighterData)
  }

  async loadSelectedFighter ({ detail: { fighter } }) {
    let fighterDetails = this.fightersDetailsMap.get(fighter._id)

    if (typeof fighterDetails === 'undefined') {
      fighterDetails = await fighterService.getFighterDetails(fighter._id)
      this.fightersDetailsMap.set(fighter._id, fighterDetails)
    }
  }
}

export default FightersView
