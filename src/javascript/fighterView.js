import View from './view'

class FighterView extends View {
  constructor (fighter, handlers = {}) {
    super()

    this.createFighter(fighter, handlers)
  }

  createFighter (fighter, handlers) {
    const { name, source } = fighter
    const nameElement = this.createName(name)
    const imageElement = this.createImage(source)

    this.element = this.createElement({ tagName: 'div', className: 'fighter' })
    this.element.append(imageElement, nameElement)
    
    Object.keys(handlers).map(key => {
      this.element.addEventListener(key, event => handlers[key](event, fighter))
    })

  }

  createName (name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' })
    nameElement.innerText = name

    return nameElement
  }

  createImage (source) {
    const attributes = { src: source, draggable: 'true' }
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    })

    return imgElement
  }
}

export default FighterView
