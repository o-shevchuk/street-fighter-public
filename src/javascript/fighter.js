class Fighter {
  constructor ({ name, health, attack, defense, _id, areaId }) {
    this._id = _id
    this.name = name
    this.areaId = areaId
    this.health = health
    this.attack = attack
    this.defense = defense
  }
  getHitPower () {
    return this.attack * this.criticalHitChance
  }

  getBlockPower () {
    return this.defense * this.dodgeChance
  }

  get criticalHitChance () {
    return parseFloat(Math.random().toFixed(2)) + 1
  }

  get dodgeChance () {
    return parseFloat(Math.random().toFixed(2)) + 1
  }

  get isDead () {
    return this.health <= 0
  }
}

export default Fighter
