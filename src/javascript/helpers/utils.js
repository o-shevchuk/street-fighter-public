function restrictValueByWindow (value, windowValue, maxValue) {
  return windowValue - value > maxValue ? value : windowValue - maxValue
}

export { restrictValueByWindow }
