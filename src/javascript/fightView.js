import App from './app'
import View from './view'
import Button from './components/button'
import SelectArea from './components/selectArea'
import { fight } from './helpers/fight'
import Fighter from './fighter'

class FightView extends View {
  constructor () {
    super()

    this.handleSelect = this.handleFighterSelect.bind(this)
    this.handleStartButtonClick = this.handleStart.bind(this)
    this.handleHitButtonClick = this.startFightRound.bind(this)
    this.handleCompleteButtonClick = this.resetFight.bind(this)
    this.createFightView()
    this.element.addEventListener('select-fighter', this.handleSelect)
  }

  selected = new Map()

  createFightView () {
    this.element = this.createElement({ tagName: 'div', className: 'select-container' })

    this.fightersArea = Array.apply(null, Array(2)).map((item, areaId) => {
      const selectArea = new SelectArea(areaId)
      return selectArea
    })

    const buttonContainer = this.createElement({ tagName: 'div', className: 'buttons-container' })
    this.startFightButton = new Button('Fight', this.handleStartButtonClick)
    this.hitButton = new Button('Hit', this.handleHitButtonClick)
    this.completeButton = new Button('Complete', this.handleCompleteButtonClick)
    this.hitButton.hide()
    this.completeButton.hide()

    buttonContainer.append(this.startFightButton.element, this.hitButton.element, this.completeButton.element)

    this.fightersArea.map(area => this.element.append(area.element))
    this.element.append(buttonContainer)
  }

  handleFighterSelect ({ detail }) {
    const areaId = detail.areaId
    const fighter = detail.fighter
    this.selected.set(areaId, fighter)
  }

  handleStart () {
    if (this.selected.size == 2) {
      App.rootElement.classList.add('fight')
      this.startFight(this.selected)
      this.startFightButton.hide()
      this.hitButton.show()
    }
  }

  startFight (selected) {
    const fightersDetails = window.app.fightersView.fightersDetailsMap
    let selectedFighters = []

    selected.forEach((fighter, areaId) => {
      const fighterDetails = fightersDetails.get(fighter._id)
      const fighterInstance = new Fighter({ ...fighterDetails, areaId })
      selectedFighters.push(fighterInstance)

      const currentFighterArea = this.getFighterAreaBy(areaId)
      currentFighterArea.healthBar.init(fighterDetails.health)
    })

    this.fightIterator = fight(...selectedFighters)
  }

  startFightRound () {
    const roundResult = this.fightIterator.next().value
    if (!roundResult.winner) {
      roundResult.map(hero => {
        const { _id, health, areaId } = hero.fighter
        const fighterHealthBar = this.getFighterAreaBy(areaId).healthBar
        console.log(health, hero.fighter.name)
        fighterHealthBar.set(health)
      })
    } else {
      const message = roundResult.winner != 'none' ? `${roundResult.winner.name} Won` : 'They all are Dead!'
      
      window.app.messageBox.set(message)
      this.hitButton.hide()
      this.completeButton.show()
    }
  }

  getFighterAreaBy (id) {
    return this.fightersArea.find(area => area.areaId == id)
  }

  resetFight () {
    this.fightersArea.map(area => area.healthBar.reset())
    this.completeButton.hide()
    this.startFightButton.show()
    App.rootElement.classList.remove('fight')
  }
}

export default FightView
